﻿/**
 * @author: Guilherme de Paula Silva
 * @since: 08/27/14
 * @version: 1.0
 * @description: A library to handle the basic operations
 *               on Microsoft Active Directory. 
 * @contact: guilhermesilva.sistemas@gmail.com
 * ***************************************************************
 * @update: Inclusion of a method to delete an user.
 * @since: 08/29/14
 * @author: Guilherme de Paula Silva
 * ***************************************************************
 * @update: Inclusion of a method to create organizational unit.
 * @since: 09/03/14
 * @author: Guilherme de Paula Silva
 * ***************************************************************
 * @update: Inclusion of a method to verify if there an organizational
 * unit. Inclusion of a user property display name a method to move
 * an user object to another organization unit.
 * @since: 09/09/14
 * @author: Guilherme de Paula Silva
 **/

using System;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;

namespace ADLib
{
    public class ADHandler
    {
        private string _firstParamDC;
        private string _secondParamDC;
        private string _domain;
        private PrincipalContext _principalContext;
        private string _path;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="domain"></param>
        public ADHandler(string domain)
        {
            try 
	        {
                this.Domain = domain;
                this._principalContext = new PrincipalContext(ContextType.Domain, this.Domain);
	        }
            catch (ArgumentException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Set for DC.
        /// </summary>
        public string FirstParamDC 
        {
            //get { return this._firstParamDC; }
            set { this._firstParamDC = value; }
        }

        /// <summary>
        /// Set for DC.
        /// </summary>
        public string SecondParamDC 
        {
            get { return this._secondParamDC; }
            set { this._secondParamDC = value; }
        }

        public string Domain 
        {
            get { return this._domain; }
            set { this._domain = value; }
        }

        public string Path 
        {
            get { return this._path; }
            set { this._path = value; }
        }

        /// <summary>
        /// Method to create an user in a specific AD. The domain has have
        /// to be already known. 
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="userLogonName"></param>
        /// <param name="emailAddress"></param>
        public void CreateAnUser(string firstName, string lastName, string userLogonName, string emailAddress, string password)
        {
            User user = new User();
            string[] split;

            if (!String.IsNullOrEmpty(firstName) || !String.IsNullOrWhiteSpace(firstName)) 
            {
                split = firstName.Trim().Split(' ');
                
                for(int i = 0; i < split.Length -1; i++)
                    user.FirstName += split[i] += " ";

                user.LastName = split[split.Length - 1];
                user.DisplayName = user.FirstName + user.LastName;
            }
            if (!String.IsNullOrEmpty(lastName) || !String.IsNullOrWhiteSpace(lastName))
                user.LastName = lastName;
            if (!String.IsNullOrEmpty(userLogonName) || !String.IsNullOrWhiteSpace(userLogonName)) 
            {
                user.UserLogonName = userLogonName;
                user.UserPrincipalName = userLogonName + "@" + this.Domain;
            }
            
            //if (!String.IsNullOrEmpty(emailAddress) || !String.IsNullOrWhiteSpace(emailAddress))
            //    user.EmailAddres = emailAddress;
            if (!String.IsNullOrEmpty(password) || !String.IsNullOrWhiteSpace(password))
                user.Password = password;

            user.CN = user.FirstName + " " + user.LastName;

            if (!this.UserExists(user.UserLogonName)) 
            {
                try
                {
                    this.CreateUser(user);
                }
                catch (Exception ex)
                {
                    
                    throw ex;
                }
            }
        }

        public void DeleteAnUser(string userLogonName) 
        {
            if (!String.IsNullOrEmpty(userLogonName) || !String.IsNullOrWhiteSpace(userLogonName))
                this.DeleteUser(userLogonName);
        }

        /// <summary>
        /// Create an Organizational Unit in Microsoft Active Directory.
        /// </summary>
        /// <param name="nameOU"></param>
        /// <param name="description">Optional. Something to describe your Ortanizational Unit.</param>
        public void CreateAnOU(string name, string description) 
        {
            if (!String.IsNullOrEmpty(name) ||
                !String.IsNullOrWhiteSpace(name)) 
            {
                try
                {
                    this.CreateOU(name, description);
                }
                catch (Exception ex)
                {
                    
                    throw ex;
                }
            }
        }

        public bool VerifiyIfOUExists(string father, string child) 
        {
            if (this.OUExists(child, father))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Move an object to another OU.
        /// </summary>
        /// <param name="pathObject">Object to move.</param>
        /// <param name="newOU">New object's OU.</param>
        public void MoveUsersOU(string pathObject, string newOU) 
        {
            try
            {
                this.MoveUser(pathObject, newOU);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        internal bool UserExists(string userLogonName)
        {
            UserPrincipal up = UserPrincipal.FindByIdentity(this._principalContext, userLogonName);
           
            if (up != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Adiciona um usuário em um grupo específico.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public void AddUserToGroup(string userName, string groupName)
        {
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, this.Domain)) 
                {
                    GroupPrincipal group = GroupPrincipal.FindByIdentity(pc, groupName);
                    group.Members.Add(pc, IdentityType.SamAccountName, userName);
                    group.Save();
                }
            }
            catch (DirectoryServicesCOMException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Method to create an user on Microsoft Active Directory.
        /// </summary>
        /// <param name="user">Object that represent an user and yours properties.</param>
        internal void CreateUser(User user)
        {
            UserPrincipal up;
            
            try
            {
                up = new UserPrincipal(this._principalContext); //principalContext contains domain's description
                up.GivenName = user.FirstName;
                up.Surname = user.LastName;
                up.DisplayName = user.DisplayName;
                up.EmailAddress = user.EmailAddres;
                up.UserPrincipalName = user.UserPrincipalName;
                up.SamAccountName = user.UserLogonName;
                up.SetPassword(user.Password);
                up.Enabled = true;
                up.PasswordNeverExpires = true;

                up.Save(); //confirm the creation proccess.
                
            }
            catch (InvalidOperationException ex)
            {
                throw ex;
            }
            catch (PrincipalOperationException ex)
            {
                throw ex;
            }
            catch (PasswordException ex)
            {
                throw ex;
            }
            catch (PrincipalException ex)
            {
                throw ex;
            }        
        }

        /// <summary>
        /// Delete an user from Microsoft Active Directory.
        /// </summary>
        /// <param name="userLogon">User account name.</param>
        internal void DeleteUser(string userLogon) 
        {
            UserPrincipal up = UserPrincipal.FindByIdentity(this._principalContext, userLogon);

            if (up != null) //if up is null, the current user doesn't exists.
                up.Delete();
        }

        /// <summary>
        /// Create an Organizational Unit in Microsoft Active Directory.
        /// </summary>
        /// <param name="nameOU"></param>
        /// <param name="description">Optional. Something to describe your Ortanizational Unit.</param>
        internal void CreateOU(string nameOU, string description) 
        {
            DirectoryEntry mainEntry;
            DirectoryEntry ouEntry;

            try
            {
                mainEntry = new DirectoryEntry(this._path);
                //mainEntry.RefreshCache();
                ouEntry = mainEntry.Children.Add("OU=" + nameOU, "OrganizationalUnit");
                //ouEntry.Properties["description"].Value = description;
                ouEntry.CommitChanges();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        /// <summary>
        /// Verifiy if an organizationa unit exists in Microsoft Active Directory.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>TRUE: Exists ; FALSE: Doesn't exists.</returns>
        internal bool OUExists(string child, string father) 
        {
            DirectoryEntry entry = new DirectoryEntry(father);
            DirectoryEntry childEntry;
            
            try 
	        {
                childEntry = entry.Children.Find("OU=" + child);
                return true;
	        }
	        catch (DirectoryServicesCOMException)//if had threw an exception, object haven't exists 
	        {

                return false;
	        }
        }

        /// <summary>
        /// Move an user from default container to some organizational unit.
        /// </summary>
        /// <param name="pathObjectToMove">Object to move.</param>
        /// <param name="newOU">Organizational unit destinate.</param>
        internal void MoveUser(string pathObjectToMove, string newOU) 
        {
            DirectoryEntry oldOU = new DirectoryEntry(pathObjectToMove);
            oldOU.MoveTo(new DirectoryEntry(newOU));
        }
    }

    internal class User
    {
        //SOME USER'S PROPERTIES
        private string _firstName;
        private string _lastName;
        private string _userLogonName;
        private string _emailAddress;
        private string _password;
        private string _cn;
        private string _userPrincipalName;
        private string _displayName;

        #region Getters and Setters

        public string FirstName 
        {
            get { return this._firstName; }
            set { this._firstName = value; }
        }

        public string LastName 
        {
            get { return this._lastName; }
            set { this._lastName = value; }
        }

        public string UserLogonName 
        {
            get { return this._userLogonName; }
            set { this._userLogonName = value; }
        }

        public string EmailAddres 
        {
            get { return this._emailAddress; }
            set { this._emailAddress = value; }
        }

        public string Password
        {
            get { return this._password; }
            set { this._password = value; }
        }

        public string CN 
        { 
            get { return this._cn; }
            set { this._cn = value; }
        }

        public string UserPrincipalName 
        { 
            get { return this._userPrincipalName; } 
            set { this._userPrincipalName = value; }
        }

        public string DisplayName 
        {
            get { return this._displayName; }
            set { this._displayName = value; }
        }

        #endregion
    }
}
